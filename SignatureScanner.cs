﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace SigScan
{
    class SignatureScanner
    {
        const int PROCESS_QUERY_INFORMATION = 0x0400;
        const int MEM_COMMIT = 0x00001000;
        const int PAGE_READWRITE = 0x04;
        const int PROCESS_WM_READ = 0x0010;

        [DllImport("kernel32.dll")]
        static extern IntPtr OpenProcess
             (int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll")]
        static extern bool ReadProcessMemory
        (int hProcess, int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);

        [DllImport("kernel32.dll")]
        static extern void GetSystemInfo(out SYSTEM_INFO lpSystemInfo);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern int VirtualQueryEx(IntPtr hProcess,
        IntPtr lpAddress, out MEMORY_BASIC_INFORMATION lpBuffer, uint dwLength);

        private struct MEMORY_BASIC_INFORMATION
        {
            public int BaseAddress;
            public int AllocationBase;
            public int AllocationProtect;
            public int RegionSize;
            public int State;
            public int Protect;
            public int lType;
        }

        private struct SYSTEM_INFO
        {
            public ushort processorArchitecture;
            ushort reserved;
            public uint pageSize;
            public IntPtr minimumApplicationAddress;
            public IntPtr maximumApplicationAddress;
            public IntPtr activeProcessorMask;
            public uint numberOfProcessors;
            public uint processorType;
            public uint allocationGranularity;
            public ushort processorLevel;
            public ushort processorRevision;
        }

        private static bool MaskCheck(int offset, byte[] pattern, string mask, byte[] memoryregion)
        {
            for (int i = 0; i < pattern.Length; i++)
            {
                // If the mask char is a wildcard, just continue.
                if (mask[i] == '?')
                    continue;

                // If the mask char is not a wildcard, ensure a match is made in the pattern.
                if ((mask[i] == 'x') && (pattern[i] != memoryregion[offset + i]))
                    return false;
            }

            // The loop was successful so we found the pattern.
            return true;
        }

        public static IntPtr FindPattern(Process process, byte[] pattern, string mask)
        {
            if (pattern.Length != mask.Length)
                return IntPtr.Zero;

            // getting system info for min & max app addresses
            SYSTEM_INFO sys_info = new SYSTEM_INFO();
            GetSystemInfo(out sys_info);

            IntPtr procMinAddress = sys_info.minimumApplicationAddress;
            IntPtr procMaxAddress = sys_info.maximumApplicationAddress;
            long procMinAddress_L = (long)procMinAddress;
            long procMaxAddress_L = (long)procMaxAddress;

            // opening the process for reading
            IntPtr processHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_WM_READ, false, process.Id);

            // stores information from VirtualQueryEx()
            MEMORY_BASIC_INFORMATION memBasicInfo = new MEMORY_BASIC_INFORMATION();

            int bytesRead = 0;      // number of bytes read with ReadProcessMemory

            while (procMinAddress_L < procMaxAddress_L)
            {
                // 28 = sizeof(MEMORY_BASIC_INFORMATION)
                VirtualQueryEx(processHandle, procMinAddress, out memBasicInfo, 28);

                // if this memory chunk is accessible
                if (memBasicInfo.Protect == PAGE_READWRITE && memBasicInfo.State == MEM_COMMIT)
                {
                    // buffer for ReadProcessMemory
                    byte[] buffer = new byte[memBasicInfo.RegionSize];

                    ReadProcessMemory((int)processHandle, memBasicInfo.BaseAddress, buffer, memBasicInfo.RegionSize, ref bytesRead);

                    // searching for pattern in current memory region
                    for (int i = 0; i < buffer.Length; i++)
                    {
                        if (MaskCheck(i, pattern, mask, buffer))
                        {
                            return (IntPtr)(memBasicInfo.BaseAddress + i);
                        }
                    }
                }

                // move to the next memory region
                procMinAddress_L += memBasicInfo.RegionSize;
                procMinAddress = (IntPtr)procMinAddress_L;
            }

            return IntPtr.Zero;
        }
    }

}
